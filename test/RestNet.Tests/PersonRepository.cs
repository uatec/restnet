using System.Linq;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace RestNet.Tests
{
	public class PersonRepository : IRepository<Person>
	{
		private List<Person> _data = new List<Person> {
					new Person {
						Id = "1",
						Name = "Janet"
					},
					new Person {
						Id = "2",
						Name = "John"
					}
				};

		public Person GetById(string id)
		{
			return _data.Single(p => p.Id.Equals(id));
		}

		public IEnumerable<Person> GetAll()
		{
			return _data;
		}

		public Person Post(string id, Person obj)
		{
			if ( _data.Any(p => p.Id.Equals(id)))
			{
				_data.RemoveAll(p => p.Id.Equals(id));
			}
			_data.Add(obj);
			return obj;
		}

		public Person Post(Person obj)
		{
			obj.Id = Guid.NewGuid().ToString();
			_data.Add(obj);
			return obj;
		}
	}
}