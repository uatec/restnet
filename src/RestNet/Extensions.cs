using System.Collections.Generic;
using System;

namespace RestNet
{
	public static class Extensions
	{
		public static List<Tuple<string, string>> SplitPath(this string path)
		{
			string[] pathParts = path.Split('/');
			List<Tuple<string, string>> pairs = new List<Tuple<string, string>>();

			for ( int i = 0; i < pathParts.Length -1 ; i+=2)
			{
				int typeIndex = i+1;
				string type = typeIndex < pathParts.Length ? pathParts[typeIndex] : null;
				int idIndex = i+2;
				string id = idIndex < pathParts.Length ? pathParts[idIndex] : null;
				pairs.Add(Tuple.Create(type, id));
			}

			return pairs;
		}

		public static string GetRepositoryName(this string[] pathParts, int index)
		{ 
			int partIndex = 1 + (index *2);
			if ( partIndex >= pathParts.Length)
			{
				 return null;
			}
			return pathParts[partIndex].ToLower();
		}

		public static string GetEntityId(this string[] pathParts, int index)
		{
			int partIndex = 2 + (index *2);
			if ( partIndex >= pathParts.Length )
			{
				 return null;
			}
			return pathParts[partIndex];			
		}
	}
}