using System.Linq;
using System;
using System.Collections.Generic;
using System.Reflection;
using RestNet;

namespace RestNet.Tests
{
	public class Level1GetTests
	{
		[Fact]
		public void TheTypeReturnedIsThatOfTheFirstUrlSegment()
		{
			// Arrange
			HandlerCore core = new HandlerCore(new Dictionary<string, Tuple<Type, IRepository>>
			{
				{ "person", Tuple.Create<Type, IRepository>(typeof(Person), new PersonRepository()) }
			}, new JsonSerialiser());
			// Act
			string result = core.GetResource("/Person/1");
			// Assert
			Assert.Equal("{\"Id\":\"1\",\"Name\":\"Janet\"}", result);
		}

		[Fact]
		public void NoIdReturnsAListOfType()
		{
			// Arrange
			HandlerCore core = new HandlerCore(new Dictionary<string, Tuple<Type, IRepository>>
			{
				{ "person", Tuple.Create<Type, IRepository>(typeof(Person), new PersonRepository()) }
			}, new JsonSerialiser());
			// Act
			string results = core.GetResource("/Person");
			// Assert
			Assert.Equal("[{\"Id\":\"1\",\"Name\":\"Janet\"},{\"Id\":\"2\",\"Name\":\"John\"}]", results);
		}
	}
}