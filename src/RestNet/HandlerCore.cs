using System;
using System.Collections.Generic;
using System.Linq;

namespace RestNet
{
	public class HandlerCore
	{
		private readonly Dictionary<string, Tuple<Type, IRepository>> _registeredRepositories;
		private readonly ISerialiser _serialiser;

		public HandlerCore(Dictionary<string, Tuple<Type, IRepository>> repositories,
			ISerialiser serialiser)
		{
			if (repositories == null) throw new ArgumentNullException("repositories");
			if (serialiser == null) throw new ArgumentNullException("serialiser");

			_registeredRepositories = repositories;
			_serialiser = serialiser;
		}

		public IRepository<T> GetRepository<T>(string typeName)
		{
			if ( typeName == null ) throw new ArgumentNullException("typeName");

			typeName = typeName.ToLower();
			if ( !_registeredRepositories.ContainsKey(typeName))
			{
				throw new Exception(string.Format("No repository found for type name: '{0}'", typeName));
			}
			return (IRepository<T>) _registeredRepositories[typeName.ToLower()].Item2;			
		}

		public string PostResource(string path, string serialisedObject)
		{
			var pathPairs = path.SplitPath();
			Type resourceType = _registeredRepositories[pathPairs.Last().Item1.ToLower()].Item1;
			var mi = this.GetType().GetMethod("PostResourceInner");
			var gmi = mi.MakeGenericMethod(new Type[]{ resourceType });
			string response = (string) gmi.Invoke(this, new object[] { pathPairs, serialisedObject});
			return response;
		}

		public string PostResourceInner<T>(List<Tuple<string, string>> pathPairs, string serialisedObject)
		{
			T deserialisedObject = _serialiser.Deserialise<T>(serialisedObject);
		
			string repoName = pathPairs.Last().Item1;
			IRepository<T> repository = GetRepository<T>(repoName);

			string entityId = pathPairs.Last().Item2;
			if ( entityId != null )
			{
				T responseObject = repository.Post(entityId, deserialisedObject);
				string serialisedResponse = _serialiser.Serialise(responseObject);
				return serialisedResponse;
			}
			else
			{
				T responseObject = repository.Post(deserialisedObject);
				string serialisedResponse = _serialiser.Serialise(responseObject);
				return serialisedResponse;
			}
		}

		public string GetResource(string path)
		{
			var pathPairs = path.SplitPath();
			Type resourceType = _registeredRepositories[pathPairs.Last().Item1.ToLower()].Item1;
			var mi = this.GetType().GetMethod("GetResourceInner");
			var gmi = mi.MakeGenericMethod(new Type[]{ resourceType });
			string response = (string) gmi.Invoke(this, new object[] { pathPairs });
			return response;
		}

		public string GetResourceInner<T>(List<Tuple<string, string>> pathPairs)
		{
			string repoName = pathPairs.Last().Item1;
			IRepository<T> repository = GetRepository<T>(repoName);
			string entityId = pathPairs.Last().Item2;
			if ( entityId == null )
			{
				return _serialiser.Serialise(repository.GetAll());
			}
			return _serialiser.Serialise(repository.GetById(entityId));
		}
	}
}