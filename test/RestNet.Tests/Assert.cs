namespace RestNet.Tests
{
	public static class Assert
	{
		public static void True(bool value)
		{
			Assert.True(null, value);
		}

		public static void True(string message, bool value)
		{
			if ( !value )
			{
				if ( message != null )
				{
					throw new AssertionFailedException("Expected true, but was false");
				}
				else
				{
					throw new AssertionFailedException(message);
				}
			}
		}

		public static void Equal(object expected, object actual)
		{
			if ( expected == null && actual == null ) return;
			if ( expected != null )
			{
				if ( !expected.Equals(actual)) 
				{
					throw new AssertionFailedException(string.Format("Values not equal. Expected: '{0}', Actual: '{1}'", expected, actual));
				}
			}
			else // actual != null 
			{
				if ( !actual.Equals(expected))
				{
					throw new AssertionFailedException(string.Format("Values not equal. Expected: '{0}', Actual: '{1}'", expected, actual));
				}
			}
		}
	}
}