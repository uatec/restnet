using System.Collections.Generic;

namespace RestNet
{
	public interface IRepository
	{

	}

	public interface IRepository<T> : IRepository
	{
		T GetById(string id);
		IEnumerable<T> GetAll();
		T Post(string id, T obj);
		T Post(T obj);
	}
}