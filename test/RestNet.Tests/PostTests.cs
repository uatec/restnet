using RestNet;
using System.Collections.Generic;
using System;
using Newtonsoft.Json.Linq;

namespace RestNet.Tests
{
	public class PostTests
	{
		[Fact]
		public void PostedDataIsSentToCorrectRepository()
		{
			// Arrange
			IRepository<Pet> petRepository = new PetRepository();
			HandlerCore core = new HandlerCore(new Dictionary<string, Tuple<Type, IRepository>>
			{
				{ "person", Tuple.Create<Type, IRepository>(typeof(Person), new PersonRepository()) },
				{ "pet", Tuple.Create<Type, IRepository>(typeof(Pet), petRepository) }
			}, new JsonSerialiser());
			string jsonPet = @"{
				 ""name"": ""fuzzykins""
				}";
			string url = "/person/3/pet";
			// Act
			string response = core.PostResource(url, jsonPet);
			// Assert
			dynamic responsePet = JObject.Parse(response);
			string responseId = responsePet["Id"];
			Pet storedPet = (Pet) petRepository.GetById(responseId);
			Assert.Equal("fuzzykins", storedPet.Name);
		}
	}
}