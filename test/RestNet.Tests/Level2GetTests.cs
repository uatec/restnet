using RestNet;
using System.Collections.Generic;
using System;

namespace RestNet.Tests
{
	public class Level2GetTests
	{
		[Fact]
		public void ALevel2RequestReturnsTheLevel2Type()
		{
			// Arrange
			HandlerCore core = new HandlerCore(new Dictionary<string, Tuple<Type, IRepository>>
			{
				{ "person", Tuple.Create<Type, IRepository>(typeof(Person), new PersonRepository()) },
				{ "pet", Tuple.Create<Type, IRepository>(typeof(Pet), new PetRepository()) }
			}, new JsonSerialiser());
			// Act
			string result = core.GetResource("/person/1/pet/1");
			// Assert
			Assert.Equal("{\"Id\":\"1\",\"Name\":\"Fluffy\"}", result);
		}

		[Fact]
		public void ALevel2RequestReturnsAListOfLevel2Type()
		{
			// Arrange
			HandlerCore core = new HandlerCore(new Dictionary<string, Tuple<Type, IRepository>>
			{
				{ "person", Tuple.Create<Type, IRepository>(typeof(Person), new PersonRepository()) },
				{ "pet", Tuple.Create<Type, IRepository>(typeof(Pet), new PetRepository()) }
			}, new JsonSerialiser());
			// Act
			string results = core.GetResource("/person/1/pet");
			// Assert
			Assert.Equal("[{\"Id\":\"1\",\"Name\":\"Fluffy\"},{\"Id\":\"2\",\"Name\":\"Snuggles\"}]", results);
		}
	}
}