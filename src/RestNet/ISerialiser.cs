namespace RestNet
{
	public interface ISerialiser
	{
		T Deserialise<T>(string data);
		string Serialise<T>(T obj);
	}
}