using Newtonsoft.Json;

namespace RestNet
{
	public class JsonSerialiser : ISerialiser
	{
		public T Deserialise<T>(string data)
		{
			return JsonConvert.DeserializeObject<T>(data);
		}

		public string Serialise<T>(T obj)
		{
			return JsonConvert.SerializeObject(obj);
		}
	}
}