using System;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;

namespace RestNet.Tests
{
	public class Program 
	{
		private static List<Type> getAllTypes()
		{
			try 
			{
				return typeof(Program).Assembly.GetTypes().ToList();
			}
			catch ( ReflectionTypeLoadException ex)
			{
				foreach ( Exception ex1 in ex.LoaderExceptions)
				{
					Console.WriteLine(ex1.Message);
				}
				throw ex;
			}
		}

		private static List<MethodInfo> filterForFacts(List<Type> discoveredTypes)
		{
			return discoveredTypes
				.SelectMany(t => 
				{
					return t.GetMethods()
					.Where(m => m
						.GetCustomAttributes()
						.OfType<FactAttribute>()
						.Any());
				})
				.ToList();
		}

		private static string getTestName(MethodInfo test)
		{
			return test.DeclaringType.Name + "." + test.Name;
		}

		private static void ReportTestStatus(MethodInfo test, bool pass)
		{
			if ( pass )
			{
				Console.ForegroundColor = ConsoleColor.Green;
				Console.Write("PASS");
			}
			else
			{
				Console.ForegroundColor = ConsoleColor.Red;
				Console.Write("FAIL");
			}
			Console.ForegroundColor = ConsoleColor.White;
			Console.WriteLine(": {0}", getTestName(test));
		}

		public static void Main(string[] args)
		{
			// Find all types
			List<Type> discoveredTypes = getAllTypes();
			// filter on ones that have functions with FactAttribute on them
			List<MethodInfo> tests = filterForFacts(discoveredTypes);

			Console.ForegroundColor = ConsoleColor.White;
			int testCount = tests.Count();
			int passes = 0;
			int fails = 0;
			foreach ( MethodInfo test in tests)
			{
				try 
				{
					object testInstance = Activator.CreateInstance(test.DeclaringType);

					test.Invoke(testInstance, new object[]{});

					ReportTestStatus(test, true);
					passes++;
				}
				catch (TargetInvocationException ex)
				{
					fails++;
					ReportTestStatus(test, false);
					
					if ( ex.InnerException is AssertionFailedException)
					{
						Console.WriteLine("\tAssertion failed: {0}", ex.InnerException.Message);
					}
					else
					{
						Console.WriteLine("\tUnhandled exception: {0}", ex.InnerException.Message);
					}
					Console.WriteLine("\tStackTrace: {0}", ex.InnerException.StackTrace);
					Console.WriteLine("--------------------");
				}
			}
			
			if ( passes > 0 )
			{
				Console.ForegroundColor = ConsoleColor.Green;
				Console.WriteLine("{0}/{1} tests passed.", passes, testCount);
			}

			if ( fails > 0 )
			{
				Console.ForegroundColor = ConsoleColor.Red;
				Console.WriteLine("{0}/{1} tests failed.", fails, testCount);
			}
			Console.ForegroundColor = ConsoleColor.White;
		}
	}
}