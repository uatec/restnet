using System.Linq;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace RestNet.Tests
{
	public class PetRepository : IRepository<Pet>
	{
		private List<Pet> _data = new List<Pet> {
					new Pet {
						Id = "1",
						Name = "Fluffy"
					},
					new Pet {
						Id = "2",
						Name = "Snuggles"
					}
				};

		public Pet GetById(string id)
		{
			return _data.Single(p => p.Id.Equals(id));
		}

		public IEnumerable<Pet> GetAll()
		{
			return _data;
		}

		public Pet Post(string id, Pet obj)
		{
			if ( _data.Any(p => p.Id.Equals(id)))
			{
				_data.RemoveAll(p => p.Id.Equals(id));
			}
			_data.Add(obj);
			return obj;
		}

		public Pet Post(Pet obj)
		{
			obj.Id = Guid.NewGuid().ToString();
			_data.Add(obj);
			return obj;
		}
	}
}