using System;

namespace RestNet.Tests
{
	public class AssertionFailedException : Exception 
	{
		public AssertionFailedException(string message)
			: base(message)
		{

		}
	}
}